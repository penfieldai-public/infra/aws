
# AWS variables
variable "aws_region" {
  description = "The name of the selected region"
  type        = string
  default     = "ca-central-1"
}

# ACM
variable "domain_names" {
  description = "A domain name for which the certificate should be issued"
  type        = list(string)
  default     = [] #["penfield.ai", "*.penfield.ai", "poc.penfield.ai"] # You add one domain or multiple domain or wildcard certificate
}

variable "validation_method" {
  description = "Which method to use for validation. DNS or EMAIL are valid"
  type        = string
  default     = "DNS"
}

# shared variables (used in tags)
variable "project" {
  description = "Name of the project"
  type        = string
  default     = "penfieldai"
}

variable "terraform" {
  description = "Managed by Terraform"
  type        = string
  default     = "true"
}
