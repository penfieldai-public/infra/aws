
#### ACM

output "aws_acm_certificate_domain_name" {
  description = "The domain name for which the certificate is issued"
  value       = aws_acm_certificate.acm_cert.domain_name
}

output "aws_acm_certificate_arn" {
  description = "The ARN of the certificate"
  value       = aws_acm_certificate.acm_cert.arn
}

output "aws_acm_certificate_validation_emails" {
  description = "A list of addresses that received a validation E-Mail. Only set if EMAIL-validation was used."
  value       = flatten(aws_acm_certificate.acm_cert.*.validation_emails)
}
