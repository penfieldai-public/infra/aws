# Create AWS ACM
resource "aws_acm_certificate" "acm_cert" {
  domain_name               = var.domain_names[0]
  subject_alternative_names = slice(var.domain_names, 1, length(var.domain_names))
  validation_method         = var.validation_method

  tags = {
    Terraform = "${var.terraform}"
    Project   = "${var.project}"
  }

  lifecycle {
    create_before_destroy = true
  }
}
