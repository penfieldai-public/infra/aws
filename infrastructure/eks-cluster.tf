module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 19.0"

  cluster_name                    = var.cluster_name
  cluster_version                 = var.cluster_version
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = false
  # cluster_endpoint_public_access  = true
  # cluster_endpoint_public_access_cidrs  = ["163.116.141.0/24","163.116.130.0/24","163.116.142.0/24"] # "163.116.141.0/24","163.116.130.0/24","163.116.142.0/24" are penfield IP's, you can also add your Public IP to test teh connectvity.
  cluster_enabled_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  cluster_addons = {
    aws-ebs-csi-driver = {
      most_recent = true
    }
  }
  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

  # Extend cluster security group rules
  cluster_security_group_additional_rules = {
    egress_nodes_ephemeral_ports_tcp = {
      description                = "cluster to node 1025-65535"
      protocol                   = "tcp"
      from_port                  = 1025
      to_port                    = 65535
      type                       = "egress"
      source_node_security_group = true
    }
  }

  # Extend node-to-node security group rules
  node_security_group_additional_rules = {
    ingress_self_all = {
      description = "Node to node all ports/protocols"
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      type        = "ingress"
      self        = true
    }
    ingress_alb = {
      description              = "ALB to node on 31080 nodeport"
      protocol                 = "tcp"
      from_port                = 31080
      to_port                  = 31080
      type                     = "ingress"
      source_security_group_id = aws_security_group.eks_alb_sg.id
    }
  }

  # EKS Managed Node Group(s)
  eks_managed_node_group_defaults = {
    ami_type  = "AL2_x86_64"
    disk_size = var.disk_size
  }

  eks_managed_node_groups = {
    web_nodes = {
      min_size     = var.min_size
      max_size     = var.max_size
      desired_size = var.desired_size

      instance_types = var.instance_types
      capacity_type  = var.capacity_type

      create_security_group = false

      tags = {
        Terraform   = var.terraform
        Environment = var.environment
        Project     = var.project
        Cluster     = var.cluster_name
      }
    }
  }
}
