# AWS variables
variable "aws_region" {
  description = "The name of the selected region"
  type        = string
  # default = "ca-central-1"
  default = "us-east-1"
}

# VPC Variables
variable "vpc_name" {
  description = "Name of the VPC"
  type        = string
  default     = "penfieldai-vpc"
}

variable "vpc_cidr" {
  description = "CIDR block of the VPC"
  type        = string
  default     = "10.250.0.0/16"
}

variable "public_subnets" {
  description = "The list of CIDR blocks to use in building the public subnets"
  type        = list(string)
  default = [
    "10.250.0.0/20",
    "10.250.16.0/20"
  ]
}

variable "private_subnets" {
  description = "The list of CIDR blocks to use in building the private subnets"
  type        = list(string)
  default = [
    "10.250.32.0/20",
    "10.250.48.0/20"
  ]
}

variable "database_subnets" {
  description = "The list of CIDR blocks to use in building the private database subnets"
  type        = list(string)
  default = [
    "10.250.64.0/20",
    "10.250.80.0/20"
  ]
}

# EKS cluster variables

variable "cluster_name" {
  description = "Name of the EKS cluster"
  type        = string
  default     = "penfieldai-cluster"
}

variable "cluster_version" {
  description = "The version of the EKS cluster"
  type        = string
  default     = "1.29"
}

variable "disk_size" {
  description = "Size of EKS Instance disk in GB"
  type        = number
  default     = "50"
}

variable "instance_types" {
  description = "Instance type for EKS cluster, Recommended: m5.2xlarge or higher"
  type        = list(string)
  default     = ["m5.2xlarge"]
}

variable "capacity_type" {
  description = "Type of capacity associated with the EKS Node Group. Valid values: ON_DEMAND, SPOT"
  type        = string
  default     = "ON_DEMAND"
}

variable "min_size" {
  description = "Minimum number of EKS Instances"
  type        = number
  default     = "2"
}

variable "max_size" {
  description = "Maximum number of EKS Instances"
  type        = number
  default     = "5"
}

variable "desired_size" {
  description = "Desired number of EKS Instances"
  type        = number
  default     = "3"
}

# IAM user

variable "iam_user_name" {
  description = "Name of IAM user"
  type        = string
  default     = "penfieldai"
}

# ACM

variable "aws_acm_certificate_arn" {
  description = "The ARN of the certificate used for load balancer"
  type        = string
  default     = ""
}

# ALB

variable "alb_name" {
  description = "Name of the ALB"
  type        = string
  default     = "penfieldai-cluster-internal-alb"
}

variable "internal" {
  description = "If true, the LB will be internal"
  type        = string
  default     = true
}

variable "node_port" {
  description = "Port on which targets receive traffic"
  type        = number
  default     = "31080"
}

# Aurora DB variables

variable "create_aurora_psql_cluster" {
  description = "Whether cluster should be created (affects nearly all DB resources)"
  type        = string
  default     = "false"
}

variable "aurora_psql_cluster_name" {
  description = "Name of the aurora DB cluster"
  type        = string
  default     = "penfieldai-eks-postgres-db"
}

variable "aurora_psql_engine_version" {
  description = "Version of the aurora postgres DB engine"
  type        = string
  default     = "13.6"
}

variable "aurora_psql_instance_class" {
  description = "Instance type to use for writer and reader instance, Recommended: 4 CPU, 32 GB memory, 200GB SSD with expandable storage"
  type        = string
  default     = "db.r5.xlarge"
}

variable "aurora_psql_allowed_security_groups" {
  description = "A list of Security Group ID's to allow access to"
  type        = list(string)
  default     = []
}

variable "aurora_psql_deletion_protection" {
  description = "If the DB instance should have deletion protection enabled. The database can't be deleted when this value is set to `true`. The default is `true`"
  type        = bool
  default     = true
}

variable "aurora_psql_backup_retention_period" {
  description = "The days to retain backups for. Default `34`"
  type        = number
  default     = 34
}

variable "aurora_psql_preferred_backup_window" {
  description = "The daily time range during which automated backups are created if automated backups are enabled using the `backup_retention_period` parameter. Time in UTC"
  type        = string
  default     = "04:00-04:30"
}

variable "aurora_psql_preferred_maintenance_window" {
  description = "The weekly time range during which system maintenance can occur, in (UTC)"
  type        = string
  default     = "sun:05:00-sun:06:00"
}

# shared variables (used in tags)
variable "environment" {
  description = "Name of the environment"
  type        = string
  default     = "prod"
}

variable "project" {
  description = "Name of the project"
  type        = string
  default     = "penfieldai"
}

variable "terraform" {
  description = "Managed by Terraform"
  type        = string
  default     = "true"
}
