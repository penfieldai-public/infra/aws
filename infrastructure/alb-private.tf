# ALB  security group

resource "aws_security_group" "eks_alb_sg" {
  name_prefix = "${var.cluster_name}-internal-alb-sg-"
  description = "Security group for alb of the EKS cluster"
  vpc_id      = module.vpc.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = "true"
  }

  tags = {
    Name        = "${var.cluster_name}-internal-alb-sg"
    Terraform   = "${var.terraform}"
    Environment = "${var.environment}"
    Project     = "${var.project}"
  }
}

resource "aws_security_group_rule" "eks_alb_self_inbound_sg_rule" {
  description              = "Self inbound sg group"
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "-1"
  security_group_id        = aws_security_group.eks_alb_sg.id
  source_security_group_id = aws_security_group.eks_alb_sg.id
}

resource "aws_security_group_rule" "eks_alb_vpc_https_sg_rule" {
  description       = "Allow HTTPS communication from network VPC"
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.eks_alb_sg.id
  cidr_blocks       = [module.vpc.vpc_cidr_block]
}
#
# resource "aws_security_group_rule" "eks_alb_vpn_sg_rule" {
# description              = "Allow HTTPS communication from VPN"
# type                     = "ingress"
# from_port                = 443
# to_port                  = 443
# protocol                 = "tcp"
# security_group_id        = aws_security_group.eks_alb_sg.id
# source_security_group_id = "${var.vpn_sg_id}"
# }

# Create ALB

resource "aws_lb" "eks_alb" {
  name               = var.alb_name
  internal           = var.internal
  load_balancer_type = "application"
  security_groups    = [aws_security_group.eks_alb_sg.id]
  subnets            = module.vpc.private_subnets

  enable_deletion_protection = false

  tags = {
    Name        = "${var.alb_name}"
    Terraform   = "${var.terraform}"
    Environment = "${var.environment}"
    Project     = "${var.project}"
  }
}

# Create target group
resource "aws_lb_target_group" "alb_tg" {
  name     = var.alb_name
  port     = var.node_port
  protocol = "HTTP"
  vpc_id   = module.vpc.vpc_id

  lifecycle {
    create_before_destroy = true
  }

  health_check {
    enabled             = "true"
    protocol            = "HTTP"
    path                = "/healthz"
    healthy_threshold   = "2"
    unhealthy_threshold = "2"
    interval            = "5"
    timeout             = "2"
    matcher             = "200"
  }

  tags = {
    Terraform   = "${var.terraform}"
    Environment = "${var.environment}"
    Project     = "${var.project}"
  }
}

# get EKS auto scaling group
data "aws_autoscaling_groups" "groups" {
  filter {
    name   = "tag:eks:cluster-name"
    values = ["${var.cluster_name}"]
  }

  depends_on = [
    module.eks.eks_managed_node_groups
  ]
}

# Attach auto scaling group with target group
resource "aws_autoscaling_attachment" "alb_asg_attachment" {
  # autoscaling_group_name = data.aws_autoscaling_groups.groups.names[0]
  autoscaling_group_name = element(data.aws_autoscaling_groups.groups.names, length(data.aws_autoscaling_groups.groups.names) - 1)
  lb_target_group_arn    = aws_lb_target_group.alb_tg.arn

  depends_on = [
    module.eks.eks_managed_node_groups
  ]
}

# Create alb listener
resource "aws_lb_listener" "alb_listener_80" {
  load_balancer_arn = aws_lb.eks_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: Oops! That's an Error"
      status_code  = "404"
    }
  }
}

resource "aws_lb_listener" "alb_listener_443" {
  load_balancer_arn = aws_lb.eks_alb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
  certificate_arn   = var.aws_acm_certificate_arn

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: Oops! That's an Error"
      status_code  = "404"
    }
  }
}

# Create ALB Listener Rule
resource "aws_lb_listener_rule" "alb_listener_rule_80" {
  listener_arn = aws_lb_listener.alb_listener_80.arn
  priority     = "1"

  action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
      host        = "#{host}"  # "#{host}" is default value
      path        = "/#{path}" # "/#{path}" is default value
      query       = "#{query}" # "#{query}" is default value
    }
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }
}

resource "aws_lb_listener_rule" "alb_listener_rule_443" {
  listener_arn = aws_lb_listener.alb_listener_443.arn
  priority     = "1"


  condition {
    #to do: to use host name only
    # host_header {
    #   values = ["*.dev.can1.penfield.ai"]
    # }
    path_pattern {
      values = ["/*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_tg.arn
  }
}
