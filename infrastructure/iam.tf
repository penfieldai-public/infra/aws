# Read GPG key froom local file
data "local_file" "penfield_gpg_key" {
  filename = "./data/penfield-public-key-base64.gpg"
}

# Create IAM User
resource "aws_iam_user" "penfieldai_iam_user" {
  name = var.iam_user_name

  tags = {
    Terraform = var.terraform
    Project   = var.project
  }
}

# Create access key and secret key for IAM user
resource "aws_iam_access_key" "penfieldai_iam_access_key" {
  user    = aws_iam_user.penfieldai_iam_user.name
  pgp_key = data.local_file.penfield_gpg_key.content
}

# Attach policy to IAM user
resource "aws_iam_user_policy" "penfieldai_eks_policy" {
  name = "eks_cluster_access"
  user = aws_iam_user.penfieldai_iam_user.name

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Action" : [
          "eks:*"
        ],
        "Effect" : "Allow",
        "Resource" : "${module.eks.cluster_arn}"
      },
      {
        "Action" : [
          "secretsmanager:GetResourcePolicy",
          "secretsmanager:GetSecretValue",
          "secretsmanager:DescribeSecret",
          "secretsmanager:ListSecretVersionIds"
        ],
        "Effect" : "Allow",
        "Resource" : "${aws_secretsmanager_secret.penfieldai-eks-postgres-db-secret.arn}"
      },
      {
        "Action" : "secretsmanager:ListSecrets",
        "Effect" : "Allow",
        "Resource" : "*"
      },
    ]
  })
}
