# This is just an example config, that stores terraform state file on local disk. 
# Please use proper backend like remote backend S3, Terraform cloud etc
# to store your terraform state file securely on remote location

terraform {
  backend "local" {
    # path = "relative/path/to/terraform.tfstate"
  }
}
