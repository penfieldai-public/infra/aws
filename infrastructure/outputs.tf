# VPC
output "vpc_name" {
  description = "The name of the VPC specified as argument to this module"
  value       = module.vpc.name
}

output "vpc_azs" {
  description = "A list of availability zones specified as argument to this module"
  value       = module.vpc.azs
}

output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}

output "vpc_arn" {
  description = "The ARN of the VPC"
  value       = module.vpc.vpc_arn
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = module.vpc.vpc_cidr_block
}

output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = module.vpc.public_subnets
}

output "public_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value       = module.vpc.public_subnets_cidr_blocks
}

output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = module.vpc.private_subnets
}

output "private_subnets_cidr_blocks" {
  description = "List of cidr_blocks of private subnets"
  value       = module.vpc.private_subnets_cidr_blocks
}

output "database_subnets" {
  description = "List of IDs of database subnets"
  value       = module.vpc.database_subnets
}

output "database_subnets_cidr_blocks" {
  description = "List of cidr_blocks of database subnets"
  value       = module.vpc.database_subnets_cidr_blocks
}


output "nat_public_ips" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"
  value       = module.vpc.nat_public_ips
}

#### EKS cluster

output "eks_cluster_name" {
  description = "The name of EKS cluster"
  value       = var.cluster_name
}

output "eks_cluster_arn" {
  description = "ARN of EKS cluster"
  value       = module.eks.cluster_arn
}

output "aws_auth_configmap_yaml" {
  description = "Formatted yaml output for base aws-auth configmap containing roles used in cluster node groups/fargate profiles"
  value       = module.eks.aws_auth_configmap_yaml
}

#### IAM user

output "penfieldai_iam_user_name" {
  description = "IAM user name"
  value       = aws_iam_user.penfieldai_iam_user.name
}

output "penfieldai_iam_user_arn" {
  description = "IAM user ARN"
  value       = aws_iam_user.penfieldai_iam_user.arn
}

output "penfieldai_iam_access_key_ID" {
  description = "Base64 encoded IAM user access key ID"
  value       = aws_iam_access_key.penfieldai_iam_access_key.id
}

output "penfieldai_iam_access_key_secret" {
  description = "Base64 encoded IAM user access key secret"
  value       = aws_iam_access_key.penfieldai_iam_access_key.encrypted_secret
}

# ALB

output "penfieldai_cluster_internal_alb_id" {
  description = "IAM user name"
  value       = aws_lb.eks_alb.id
}

output "penfieldai_cluster_internal_alb_arn" {
  description = "IAM user name"
  value       = aws_lb.eks_alb.arn
}

output "penfieldai_cluster_internal_alb_dns_name" {
  description = "IAM user name"
  value       = aws_lb.eks_alb.dns_name
}


# # Aurora DB output
#  ## aws_db_subnet_group
# output "aurora_db_subnet_group_name" {
#   description = "The db subnet group name"
#   value       = module.aurora_psql_cluster.db_subnet_group_name
# }
#
#  ## aws_rds_cluster
# output "aurora_cluster_arn" {
#   description = "Amazon Resource Name (ARN) of cluster"
#   value       = module.aurora_psql_cluster.cluster_arn
# }
#
# output "aurora_cluster_endpoint" {
#   description = "Writer endpoint for the cluster"
#   value       = module.aurora_psql_cluster.cluster_endpoint
# }
#
# output "aurora_cluster_reader_endpoint" {
#   description = "A read-only endpoint for the cluster, automatically load-balanced across replicas"
#   value       = module.aurora_psql_cluster.cluster_reader_endpoint
# }
#
# output "aurora_cluster_engine_version_actual" {
#   description = "The running version of the cluster database"
#   value       = module.aurora_psql_cluster.cluster_engine_version_actual
# }
# ## aws_rds_cluster_instances
# output "aurora_cluster_instances" {
#   description = "A map of cluster instances and their attributes"
#   value       = module.aurora_psql_cluster.cluster_instances
# }
