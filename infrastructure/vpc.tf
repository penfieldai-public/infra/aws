// Configure AWS VPC, Subnets, and Routes
data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name = var.vpc_name
  cidr = var.vpc_cidr

  azs              = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  public_subnets   = var.public_subnets
  private_subnets  = var.private_subnets
  database_subnets = var.database_subnets

  instance_tenancy       = "default"
  enable_nat_gateway     = true
  one_nat_gateway_per_az = true
  enable_dns_hostnames   = true
  enable_dns_support     = true
  # enable_vpn_gateway                = false
  # propagate_public_route_tables_vgw = false

  tags = {
    Terraform   = var.terraform
    Environment = var.environment
    Project     = var.project
  }
}
