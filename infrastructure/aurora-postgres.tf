# Create Secret containing DB master username and password.

# Create a random generated password to use in secrets.
resource "random_password" "password" {
  length           = 32
  special          = true
  override_special = "-_%@!#+:*&()"
}

# Creating a AWS secret for database master account
resource "aws_secretsmanager_secret" "penfieldai-eks-postgres-db-secret" {
  name = "penfieldai-eks-postgres-db-secret"
}

# Creating a AWS secret versions for database master account
resource "aws_secretsmanager_secret_version" "penfieldai-eks-postgres-db-secret-version" {
  secret_id     = aws_secretsmanager_secret.penfieldai-eks-postgres-db-secret.id
  depends_on    = [aws_secretsmanager_secret.penfieldai-eks-postgres-db-secret]
  secret_string = <<EOF
   {
    "username": "root",
    "password": "${random_password.password.result}"
   }
EOF
}

# Importing the AWS secrets created previously using arn.
data "aws_secretsmanager_secret" "penfieldai-eks-postgres-db-secret" {
  arn = aws_secretsmanager_secret.penfieldai-eks-postgres-db-secret.arn
}

# Importing the AWS secret version created previously using arn.
data "aws_secretsmanager_secret_version" "penfieldai-eks-postgres-db-creds" {
  secret_id = data.aws_secretsmanager_secret.penfieldai-eks-postgres-db-secret.arn
}

# After importing the secrets storing into Locals
locals {
  db_creds = jsondecode(data.aws_secretsmanager_secret_version.penfieldai-eks-postgres-db-creds.secret_string)
}

# Aurora postgres cluster

module "aurora_psql_cluster" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "~> 6.0"

  # Enable/Disable creation of cluster and all resources
  create_cluster = var.create_aurora_psql_cluster

  name           = var.aurora_psql_cluster_name
  engine         = "aurora-postgresql"
  engine_version = var.aurora_psql_engine_version
  instance_class = var.aurora_psql_instance_class
  instances = {
    one = {
      identifier = "instance-1"
    }
    two = {
      identifier = "instance-2"
    }
  }

  vpc_id                 = module.vpc.vpc_id
  create_db_subnet_group = false
  db_subnet_group_name   = module.vpc.database_subnet_group_name
  subnets                = module.vpc.database_subnets

  create_security_group   = true
  allowed_security_groups = var.aurora_psql_allowed_security_groups
  allowed_cidr_blocks     = module.vpc.private_subnets_cidr_blocks

  storage_encrypted   = true
  apply_immediately   = true
  monitoring_interval = 10

  enabled_cloudwatch_logs_exports = ["postgresql"]

  deletion_protection          = var.aurora_psql_deletion_protection
  backup_retention_period      = var.aurora_psql_backup_retention_period
  preferred_backup_window      = var.aurora_psql_preferred_backup_window
  preferred_maintenance_window = var.aurora_psql_preferred_maintenance_window

  create_random_password = false
  master_username        = local.db_creds.username
  master_password        = local.db_creds.password

  tags = {
    Terraform   = var.terraform
    Environment = var.environment
    Project     = var.project
  }
}
