# Terraform for PenfieldAI infrastructure on AWS

This repository contains the code that will create certificates, VPC, NAT Gateway, LoadBalancer, EKS cluster and other required components in AWS.

## Repository structure:
```
├─ aws
    ├── README
    │
    ├── aws-auth/   # Contains terraform code to update aws-auth ConfigMap in EKS cluster
    │    ├── backend.tf      # Configure terraform backend to store state file
    │    ├── providers.tf    # Configure the terraform providers
    │    ├── terraform.tf    # configure the terraform and provider versions
    │    ├── variables.tf    # Set up the variables
    │    └── configmap.tf    # EKS config map update configuration
    │
    ├── certificates/   # Contains terraform code to create certificates in AWS ACM
    │    ├── backend.tf      # Configure terraform backend to store state file
    │    ├── providers.tf    # Configure the terraform providers
    │    ├── terraform.tf    # configure the terraform and provider versions
    │    ├── variables.tf    # Set up the variables
    │    ├── outputs.tf      # Required output of code
    │    └── acm.tf          # AWS ACM configuration
    │
    ├── infrastructure
    │    ├── backend.tf          # Configure terraform backend to store state file
    │    ├── providers.tf        # Configure the terraform providers
    │    ├── terraform.tf        # configure the terraform and provider versions
    │    ├── variables.tf        # Set up the variables
    │    ├── outputs.tf          # Required output of code
    │    ├── vpc.tf              # AWS VPC configuration
    │    ├── iam.tf              # AWS IAM configuration
    │    ├── eks-cluster.tf      # AWS EKS configuration
    │    ├── alb-private.tf      # AWS ALB configuration
    │    ├── aurora-postgres.tf  # AWS Aurora PostgreSQL DB
    │    └── data/               # Contains code that is needed in terraform code
    │
    ├── diagrams/                # Contains diagrams for documentation purpose
    │
    └── sample/                  # Contains the sample code to fix EKS cluster aws-auth ConfigMap

```

---
## Infrastructure diagram

Following infrastructure will be creaetd using the terraform code in this directory:

![Alt AWS Infrastructure](./diagrams/aws-infra.jpg)

---

## Prerequisite:

* **Fully Qualified Domain Name:** Fully qualified domain name is needed that will be used to point to loadbalancer which will be the only entry point of all the traffic to EKS cluster.

* **VPN:** Since everything is setup in a private network, you (Analyst and Manager or infrastructure admin whoever need to access or manage) will need VPN solution to access private resources (cluster and loadbalancer endpoint/DNS) and to grant the access to Penfield for this cluster and endpoint.

---

## Infrastructure setup

### 1. Fully Qualified Domain Name:

Once you know what is the fully qualified domain name you would like to use for this project. Please keep it handy. For example: `https://demo.penfield.ai`

### 2. Create and validate certificate:

Once you have domain name, you need to create and validate certificates in AWS ACM. Certificates are free to create in AWS. You can either create certificate either manually or using terraform. Terraform approach is recommended. For validation part you have to do it manually depends on your DNS provider or if you choose to validate using EMAIL.

In order to create certificate using terraform, go to [certificates](./certificates) directory and edit  `variables.tf` with your inputs. Following inputs are available:

#### Inputs
| Name  | Description | Type  | Default  |
|-------|-------------|-------|----------|
| `aws_region` | The name of the selected region | `string` | `ca-central-1` |
| `domain_names` | A domain name for which the certificate should be issued | `list(string)` | `[]` | yes |
| `validation_method` | Which method to use for validation. `DNS` or `EMAIL` are valid. <br /><br />If you want to use EMAIL validation method make sure that you have access to at least one of these emails in your domain:<br /> `administrator@your_domain_name` <br /> `hostmaster@your_domain_name` <br /> `postmaster@your_domain_name` <br /> `webmaster@your_domain_name` <br /> `admin@your_domain_name` | `string` | `DNS` |

**Terraform commands:**
* `terraform init`
* `terraform plan`
* `terraform apply`

Once you create certificate, Please go to AWS ACM console  https://console.aws.amazon.com/acm/ You will see the certificate that has been created, Please validate the certificate accordingly the option you choose either via DNS or EMAIL.

#### Output (that will be needed in next step)

| Name  | Description | Required in next step  |
|-------|-------------|------------------------|
| `aws_acm_certificate_arn` | The ARN of certificate created | `yes` |

### 3. Create Infrastructure:

This directory contains all the terraform code to setup all the Infrastructure components that includes VPC,  (public and private), Route tables, NAT Gateway, Internet Gateway, Elastic IP, Application Load Balancer, EKS cluster, EKS Managed Node Group, Auto Scaling group, Security groups, Aurora PostgreSQL cluster, IAM user, roles and policies that are required. Please refer to the [Repository structure](#repository-structure) to see the purpose of various files.

**NOTES:**
* EKS cluster endpoint will be created as private, if you want to create public endpoint, go to [eks-cluster.tf](./infrastructure/eks-cluster.tf#L8), comment `Line #8` and uncomment `Line #9, #10` and provide the CIDR block to which public endpoint will be accessible (By default IP listed belongs to PenfieldAI VPN). We recommend to keep EKS cluster endpoint private and access it via your company managed VPN.
* You will need VPN solution to access internal resources and to grant the access to this cluster and endpoint. If you do not implemented any please do so.
* You can disable the Aurora PostgreSQL cluster creations if you set `create_aurora_psql_cluster` `false` in `variables.tf` file.
* Aurora PostgreSQL cluster will only be accessible from EKS nodes, if you need to grant access other security groups or VPN security group, Please specify them in `aurora_psql_allowed_security_groups` variable in `variables.tf` file.
* When you create an Amazon EKS cluster, the AWS Identity and Access Management (IAM) entity user or role, such as a federated user that creates the cluster, is automatically granted `system:masters` permissions in the cluster's role-based access control (RBAC) configuration in the Amazon EKS control plane. This IAM entity doesn't appear in any visible configuration. That is why we create new dedicated IAM user, so we can use that user to manage the cluster.
* To grant additional AWS users or roles the ability to interact with your cluster, you must edit the aws-auth ConfigMap within Kubernetes. Please refer to
[step 4](#4-aws-auth-configmap) to update aws-auth ConfigMap.

In order to create the rest of the infrastructure components, go to [infrastructure](./infrastructure) directory and edit `variables.tf` file with you inputs. Following inputs are available, (We recommend you to keep the default values though if that does not cause any interference):

#### Inputs
| Name  | Description | Type  | Default  |
|-------|-------------|-------|----------|
| `aws_region` | The name of the selected region | `string` | `ca-central-1` |
| `vpc_name` | Name of the VPC | `string` | `penfieldai-vpc` |
| `vpc_cidr` | CIDR block of the VPC | `string` | `10.250.0.0/16` |
| `public_subnets` | The list of CIDR blocks to use in building the public subnets | `list(string)` | `["10.250.0.0/20", "10.250.16.0/20"]` |
| `private_subnets` | The list of CIDR blocks to use in building the private subnets | `database_subnets` | The list of CIDR blocks to use in building the database subnets | `list(string)` | `["10.250.64.0/20", "10.250.80.0/24"]` |
| `cluster_name` | Name of the EKS cluster | `string` | `penfieldai-cluster` |
| `cluster_version` | The version of the EKS cluster | `string` | `1.21` |
| `disk_size` | Size of EKS Instance disk in GB | `number` | `50` |
| `instance_types` | Instance type for EKS cluster, Recommended: m5.2xlarge or higher" | `list(string)` | `m5.2xlarge` |
| `capacity_type` | Type of capacity associated with the EKS Node Group. Valid values: ON_DEMAND, SPOT. | `string` | `ON_DEMAND` |
| `min_size` | Minimum number of EKS Instances | `number` | `2` |
| `max_size` | Maximum number of EKS Instances | `number` | `5` |
| `desired_size` | Desired number of EKS Instances | `number` | `3` |
| `iam_user_name` | Name of IAM user | `string` | `penfieldai` |
| `aws_acm_certificate_arn` | The ARN of the certificate used for load balancer | `string` | `""` |
| `alb_name` | Name of the ALB | `string` | `penfieldai-cluster-internal-alb` |
| `internal` | If true, the LB will be internal | `string` | `true` |
| `node_port` | Port on which targets receive traffic | `number` | `31080` |
| `create_aurora_psql_cluster` | Whether Aurora postgres cluster should be created | `string` | `false` |
| `aurora_psql_cluster_name` | Name of the aurora DB cluster | `string` | `penfieldai-eks-postgres-db` |
| `aurora_psql_engine_version` | Version of the aurora postgres DB engine | `string` | `13.6` |
| `aurora_psql_instance_class` | Instance type to use for writer and reader instance | `string` | `db.r5.xlarge` |
| `aurora_psql_allowed_security_groups` | A list of Security Group ID's to allow access to| `list(string)` | `[]` |
| `aurora_psql_deletion_protection` | If the DB instance should have deletion protection enabled. | `boolean` | `true` |
| `aurora_psql_backup_retention_period` | The days to retain backups for. | `number` | `34` |
| `aurora_psql_preferred_backup_window` | The daily time range during which automated backups are created, Time in UTC | `string` | `04:00-04:30` |
| `aurora_psql_preferred_maintenance_window` | The weekly time range during which system maintenance can occur, Time in UTC | `string` | `sun:05:00-sun:06:00` |
| `environment` | Name of the environment | `string` | `prod` |
| `project` | Name of the project | `string` | `penfieldai` |
| `terraform` | Managed by Terraform | `string` | `true` |


**Terraform commands:**
* `terraform init`
* `terraform plan`
* `terraform apply`


#### Output (that will be needed in next step)

| Name  | Description | Required in next step  |
|-------|-------------|------------------------|
| `eks_cluster_name` | The name of the EKS cluster | `yes` |
| `penfieldai_iam_user_name` | IAM user name | `yes` |
| `penfieldai_iam_user_arn` | IAM user ARN| `yes` |


### 4. aws-auth configMap:

When you create an Amazon EKS cluster, the AWS Identity and Access Management (IAM) entity user or role, such as a federated user that creates the cluster, is automatically granted `system:masters` permissions in the cluster's role-based access control (RBAC) configuration in the Amazon EKS control plane. This IAM entity doesn't appear in any visible configuration, so make sure to keep track of which IAM entity originally created the cluster. To grant additional AWS users or roles the ability to interact with your cluster, you must edit the aws-auth ConfigMap within Kubernetes.

For reference follow this AWS documentation: [Enabling IAM user and role access to your cluster](https://docs.aws.amazon.com/eks/latest/userguide/add-user-role.html) You might have to start from Step 4 in AWS documentation **Edit the aws-auth ConfigMap**, But it good idea to read the full documentation.

There are two ways to update aws-auth configMap:
* Update using kubectl
* Update using terraform

**NOTE (Prerequisite):**

* You will need to use the same AWS user account that is used to create the EKS cluster in order to perform below steps:

* You need to setup kubectl config on your local: Follow this doc to setup: https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html

* You will need VPN to update your EKS cluster or need to expose the EKS endpoint to public ip.

#### Update using kubectl:

* In a text editor, open a sample file in an [aws-auth-cm.yaml](./sample/aws-auth-cm.yaml) in a [sample](./sample) directory.
* Edit the existing aws-auth ConfigMap in an EKS cluster using the command:
`kubectl edit configmap -n kube-system aws-auth`
* You have to add [line 13-17](./sample/aws-auth-cm.yaml#L13-17) as per the sample file. Make sure of proper indentation since this is YAML config. Improper indentation can cause bad configuration. You will need `<ARN of IAM user>` from the terraform output in Step 3.


#### Update using terraform:

* Go to [aws-auth](./aws-auth) directory.
* Edit `variables.tf` file with correct values of `cluster_name, iam_user_name, iam_user_arn`. You can get these from the output of Step 3, If you did not change default values in [Step 3](#3-create-infrastructure) then you will only need to update `iam_user_arn`.
* Run `terraform apply` to create the state file, it will throw error `Error: configmaps "aws-auth" already exists` which is expected.
* Import existing EKS configMap as a resource in terraform state file: `terraform import kubernetes_config_map.aws_auth_config_map kube-system/aws-auth`
* Run `terraform apply` commands.
* Verify the config map using: `kubectl describe configmap -n kube-system aws-auth`


##### Inputs
| Name  | Description | Type  | Default  |
|-------|-------------|-------|----------|
| `aws_region` | The name of the selected region | `string` | `ca-central-1` |
| `cluster_name` | Name of the EKS cluster | `string` | `penfieldai-cluster` |
| `iam_user_name` | Name of the IAM user | `string` | `penfieldai` |
| `iam_user_arn` | ARN of the IAM user | `string` | `""` |

**Terraform commands:**
* `terraform init`
* `terraform plan`
* `terraform apply`

### 5. DNS Setup:

Now it is time to point the Domain name to the ALB dns that is created in [Step 3](#3-create-infrastructure). Terraform will provide the valeu of ALB DNS in the output `penfieldai_cluster_internal_alb_dns_name` which should be used to point the Domain name chosen in the [Step 1](#1-fully-qualified-domain-name)


---

## What needs to be share with Penfield?

In the [Step 3](#3-create-infrastructure) terraform output will generate some information that you will need to share with PenfieldAI team securely, in order to give us the access to the cluster to setup the Penfield product. `penfieldai_iam_access_key_secret` is already encrypted with GPG key of Penfield. But as an other extra layer of security, Please share these `penfieldai_iam_access_key_ID`, `penfieldai_iam_access_key_secret`, `penfieldai_iam_user_arn`, `penfieldai_iam_user_name` with Penfield team using some secure channel.

**Sample output that needs to be shared:**
```
penfieldai_iam_access_key_ID = "AKICVRDQKZTQ9H2HE8UL"
penfieldai_iam_access_key_secret = "wcZXA4nwhyO1mhteARAAjejNMEnQ5X6wWQrqgkoyhwZsCu0V1QLDrbYmgLSPlfylEH/4EdnH7GK+T3agewZ9TL/OinGJZolrOGcHFCVxQxJ+pN9Cadg4KMHoUaXmXc5uYnuhq2Sy95ODasZ8GY8N4eTc6zai0jTneebIn/qKyF8syeVEJwToSaQKjUGqdckyXu7wbFWeLptMOgPmcuL1bcCHNg/+/X8WyGcC5TGLxDbz9NXEjZzE19CACczltc8reGbGDliaoZ6AJfoQh7NbUiuaWy7+sp5Bicg6OywsSouDOqhV5QGdts3k3vc1AEQQqRt6N9KjNnO2HqP2tUJ1QvkpYLomUm3wEThgPHrnLNb4G3e/qCCoBnbVTnQ0DTUmfZwdK2Kp7TasbK4L8EgQ/5phrjASBMpeiG4+0KIxqyWNQR1u7uiWvRBbtbAK/rC6Y7fhF1WTki1sip98UBSZ07pmqwiVbGHjMerKr4c3sXchj4l3xvD2JFwTNXEgXj5rGsOrF80NOPwRf0m/KQflY7bwRmJiqDl7fsPdVNjtj+drxm9Ig9eBTj02g551m1UuV6nH3pfQaag5KLoWRR4pJHCw25mSvwj/CgAcXl2q5U6SSSR2EZAyd1PuBKbtbZsicbnVjiawaM7H5Hz3mKtQ6T+ic/jaKNxcoZJRZcg5OfnbuCI7iRvTX5quRSZ31evS4AHkD7fzYVYFkTpGfJZz3HdhPOFufeCx4I7hDcfgGeKYuWDE4Jbl/xKmtre22o7x9NLLD25hLXR4smYFBOR9YFl5GHAQYKjgFeMTTKSe41YKUuDt5H4imrFGRPBSTPEbsrsg5MbiJJj+e+ESqZZ="
penfieldai_iam_user_arn = "arn:aws:iam::123456789:user/penfieldai"
penfieldai_iam_user_name = "penfieldai"
```
