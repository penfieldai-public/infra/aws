
# AWS variables
variable "aws_region" {
  description = "The name of the selected region"
  type        = string
  default     = "ca-central-1"
}

# EKS cluster variables

variable "cluster_name" {
  description = "Name of the EKS cluster"
  type        = string
  default     = "penfieldai-cluster"
}
# IAM user

variable "iam_user_name" {
  description = "Name of the IAM user"
  type        = string
  default     = "penfieldai"
}

variable "iam_user_arn" {
  description = "ARN of the IAM user"
  type        = string
  default     = "<IAM User ARN>"
}
