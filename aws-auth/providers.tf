# Configure the AWS Provider
provider "aws" {
  region = var.aws_region
}

# Configure the Kubernetes Provider
#
data "aws_eks_cluster" "eks_cluster" {
  name = var.cluster_name
}
data "aws_eks_cluster_auth" "cluster_auth" {
  name = var.cluster_name
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks_cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks_cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster_auth.token
}
