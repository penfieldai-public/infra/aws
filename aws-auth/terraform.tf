# Set the AWS Provider source and version being used
terraform {
  required_version = "~> 1.0" # Terraform CLI version

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0" # Terraform AWS provider version
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.0" # Terraform Kubernetes provider version
    }
  }
}
