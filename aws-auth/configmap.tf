
resource "kubernetes_config_map" "aws_auth_config_map" {
  provider = kubernetes

  metadata {
    name      = "aws-auth"
    namespace = "kube-system"
  }

  data = {
    mapUsers = <<ROLES
- userarn: ${var.iam_user_arn}
  username: ${var.iam_user_name}
  groups:
    - system:masters
ROLES
  }

  lifecycle {
    ignore_changes = [data["mapRoles"]]
  }
}
